/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.patthamawan.usermanagment;


import java.util.ArrayList;

/**
 *
 * @author Oil
 */
public class UserService {
     private static ArrayList<User> userlist = new ArrayList<>();
    //mockup
    static{
        userlist.add(new User("admin","password"));
         userlist.add(new User("User1","password"));
         load();
    }
    //create
    public static boolean addUser(User user){
        userlist.add(user);
        return true;
    }
    public static boolean addUser(String username , String password){
        userlist.add(new User(username , password));
        return true;
    }
    public static boolean updateUser(int index, User user){
        userlist.set(index, user);
        return true;
        
    }
    //read 1 user
     public static User getUser(int index){
         if(index>userlist.size()-1){
             return null;
         }
         return userlist.get(index);
     }
     //read all user
     public static ArrayList<User> getUsers(){
         return userlist;
     }
     //search username
     public static ArrayList<User> searchUserName(String searchText){
         ArrayList<User> list = new ArrayList<>();
         for(User user: userlist){
             if(user.getUsername().startsWith(searchText)){
                 list.add(user);
             }
         }
         return userlist;
     }
     //Delete user
     public static boolean delUser(int index){
         save();
         userlist.remove(index);
         return true;
     }
     //Delete user
     public static boolean delUser(User user){
         
         userlist.remove(user);
         return true;
     }
     //Login
     public static User login(String userName, String password){
         for(User user: userlist){
             if(user.getUsername().equals(userName) && user.getPassword().equals(password)){
             return user;
         }
         }
         return null;
     }
     public static void save() {
         
        
    }
     public static void load() {
         
        
    }

}
